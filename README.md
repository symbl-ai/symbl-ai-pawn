# Symbl.ai PAWN #



### What is Symbl.ai PAWN? ###

**Symbl.ai PAWN** (**P**ostman **A**PI **W**orkspace **N**exus) is a public workspace consisting of **Symbl.ai** APIs, SDKs, documentation, integrations, and web apps.